var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var mongoose = require("mongoose");
var busboyBodyParser = require('busboy-body-parser')
var morgan = require('morgan');


var app = express();
var port = process.env.PORT || 3000;
var server = app.listen(port);

var clients = new Array();
var io = require('socket.io').listen(server);
require('./app/config/socket.io')(io, clients);
require('./app/config/db');
require('./app/config/passport');
var routesApi = require('./app/routes/index')(io, clients);

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(busboyBodyParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use('/api', routesApi);

app.use(function(req, res){
	res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.use(function(req, res, next){
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});


app.use(function(err, req, res, next){
	if(err.name==='UnauthorizedError'){
		res.status(401);
		res.json({'message': err.name + ":" + err.message});
	}
});

console.log('Listening on port ' + port + '...');