module.exports = function(io, clients){
	

	io.sockets.on('connection', function(socket){
		
		socket.on('storeClientInfo', function(data){
			var clientInfo = new Object();
			clientInfo.customId = data.customId;
			clientInfo.socket = socket;
			clients.push(clientInfo);
			
		});
			
		

		socket.on('disconnect', function(data){
			for(var x = 0; x < clients.length; x++){
				var c = clients[x];
				if(c.socket.id == socket.id){
					clients.splice(x, 1);
					break;
				}
			}
		});

		socket.on('removeClientInfo', function(data){
			for(var x = 0; x < clients.length; x++){
				var c = clients[x];

				if(c.customId == data.customId){
					clients.splice(x, 1);
					break;
				}
			}
		});
	});

}