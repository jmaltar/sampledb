var mongoose = require('mongoose');
var GFS = mongoose.model("GFS", new mongoose.Schema({}, {strict: false}), "fs.files");
var User = mongoose.model('User');


var sampleSchema = new mongoose.Schema({
	sampleInfo: {type: mongoose.Schema.Types.Object, ref:"GFS"},
	uploadedBy: {type: mongoose.Schema.Types.ObjectId, ref:'User'},
	sampleDownloadUrl: String,
	sampleStreamUrl: String
});


var Sample = mongoose.model("Sample", sampleSchema);

module.exports = Sample;
