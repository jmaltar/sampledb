var mongoose = require('mongoose');
var Sample = mongoose.model('Sample');
var User = mongoose.model('User');
var Request = mongoose.model('Request');
var messageSchema = new mongoose.Schema({
	from: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	to: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	request: {type: mongoose.Schema.Types.ObjectId, ref:'Request'}, 
	sample: {type: mongoose.Schema.Types.ObjectId, ref: 'Sample'},
	messageDate: {type: Date, default: Date.now},
	seen: Boolean
});


var Message = mongoose.model("Message", messageSchema);

module.exports = Message;