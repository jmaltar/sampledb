var mongoose = require('mongoose');
var User = mongoose.model('User');
var requestSchema = new mongoose.Schema({
	bpm: Number,
	scales: [String],
	genres: [String],
	description: String,
	uploadDate: {type: Date, default: Date.now},
	requestedBy: {type: mongoose.Schema.Types.ObjectId, ref:'User'}
});


var Request = mongoose.model("Request", requestSchema);

module.exports = Request;

