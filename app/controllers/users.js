var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports = function(io){
	var that = {};
	const _io = io;


	var parseProperties = function(currentPage, itemsPerPage, searchModel, reverse, callback){

		var searchPropertiesInfo = new Object({
			sortProperty: new Object(),
			currentPage: parseInt(currentPage),
			itemsPerPage: parseInt(itemsPerPage)
		});

		searchPropertiesInfo.sortProperty[searchModel] = (reverse==="true") ? "desc" : "asc";

		callback(searchPropertiesInfo);

	}

	that.getUsers = function(req, res){

		parseProperties(req.query.currentPage, req.query.itemsPerPage, req.query.searchModel, req.query.reverse, function(searchPropertiesInfo){

			User
				.find()
				.count(function(err, count){

					User
						.find()
						.sort(searchPropertiesInfo.sortProperty)
						.skip((searchPropertiesInfo.currentPage - 1)*searchPropertiesInfo.itemsPerPage)
						.limit(searchPropertiesInfo.itemsPerPage)						
						.exec(function(err, users){
							if(err){
								res.status(404).json("Not found");
							}
							res.status(200).send({
								users: users,
								totalItems: count
							});
						});		

				});
		});
	}

	that.getUser = function(req, res){
		var user_id = mongoose.Types.ObjectId(req.params.user_id);
		User
			.findOne({_id: user_id})
			.exec(function(err, user){
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).json(user);
			});
	}

	return that;
}
