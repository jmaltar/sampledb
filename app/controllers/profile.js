var mongoose = require('mongoose');
var User = mongoose.model('User');
var Message = mongoose.model('Message');

module.exports = function(){
	var that = {};

	that.profileRead = function(req, res){
		if(!req.payload._id){
			res.status(401).json({
				"message": "Unauthorized access"
			});
		} else {
			User
				.findById(req.payload._id)
				.exec(function(err, user){
					if(err){
						res.status(404).json("Not found");
					}

					Message
						.find({'to': req.payload._id})
						.populate('from', 'name gravatarUrl _id')
						.exec(function(err, messages){
							if(err){
								res.status(404).json("Not found");
							}

							res.status(200).send({
								'user': user,
								'messages': messages
							});
						})
				});
		}		
	}

	that.setSeenToMessage = function(req, res){
		var m_id = mongoose.Types.ObjectId(req.body._id);
		Message
			.update({_id: m_id}, {seen: true}, function(err, nr, rawResp){
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).send({
					message: "Ok"
				});
			});		
	}

	that.deleteMessage = function(req, res){
		var m_id = mongoose.Types.ObjectId(req.params.message_id);
		if(req.headers.userid){
			var user_id = req.headers.userid;
			Message
				.findById(m_id)
				.exec(function(err, message){
					if(err){
						res.status(404).json("Not found");
					}
					if(message.to==user_id){
						Message
							.remove({_id: m_id}, function(err, removed){
								if(err){
									res.status(404).json("Not found");
								}
								return res.status(200).send({
									message: "success"
								});
							});
					} else {
						res.status(401).json({
							"message": "Unauthorized access"
						});								
					}
				});			
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});											
		}
	}


	return that;
}
