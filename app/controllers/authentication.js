var passport = require('passport');
var mongoose = require('mongoose');
var gravatar = require('gravatar');
var crypto = require('crypto');
var User = mongoose.model('User');

module.exports = function(io){
	var that = {};

	const _io = io;

	that.register = function(req, res){
		var user = new User({
			name: req.body.name,
			email: req.body.email,
			gravatarUrl: gravatar.url(req.body.email),
			memberSince: new Date()
		});

		user.setPassword(req.body.password);

		var token = user.generateJwt();
		user.save(function(err){
			if(err){
				res.status(400).json(err);
				return;
			}		
			_io.sockets.emit('user:new', user);
			res.status(200);
			res.json({
				'token': token
			});
		});		
	}

	that.login = function(req, res){
		passport.authenticate('local', function(err, user, info){
			var token;

			if(err){
				res.status(404).json(err);
				return;
			}

			if(user){
				token = user.generateJwt();
				res.status(200);
				res.json({
					"token": token
				});
			} else {
				res.status(401).json(info);
			}
		})(req, res);		
	}

	that.isUniqueEmail = function(req, res){
		var email = req.params.email;
		if(email){
			User
				.find({email: email})
				.exec(function(err, users){
						if(users.length > 0){
							res.send({"unique": false});
						} else {
							res.send({"unique": true});
						}
					});
		} else {
			res.send({"unique": true});
		}		

	}

	that.changeEmail = function(req, res){

		if(req.params.user_id==req.payload._id){
			var userId = mongoose.Types.ObjectId(req.params.user_id);
			
			var newEmail = req.body.newEmail;
			var newGravatarUrl = gravatar.url(newEmail);

			User.findByIdAndUpdate(userId, {email: newEmail, gravatarUrl: newGravatarUrl}, {new: true}, function(err, model){
				var token = model.generateJwt();
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).send({
					message: "Ok",
					token: token
				});					
			});
						
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});					
		}

	}


	that.changeName = function(req, res){

		if(req.params.user_id==req.payload._id){
			var userId = mongoose.Types.ObjectId(req.params.user_id);
			
			var newName = req.body.newName;

			User.findByIdAndUpdate(userId, {name: newName}, {new: true}, function(err, model){
				var token = model.generateJwt();
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).send({
					message: "Ok",
					token: token
				});					
			})
			
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});					
		}

	}	

	that.changePassword = function(req, res){

		if(req.params.user_id==req.payload._id){
			var userId = mongoose.Types.ObjectId(req.params.user_id);
			
			var newPassword = req.body.newPassword;

			var newSalt = crypto.randomBytes(16).toString('hex');
			var newHash = crypto.pbkdf2Sync(newPassword, newSalt,1000,64).toString('hex');


			User.findByIdAndUpdate(userId, {salt: newSalt, hash: newHash}, {new: true}, function(err, model){
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).send({
					message: "Ok"
				});					
			})
			
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});					
		}

	}		

	return that;
}
