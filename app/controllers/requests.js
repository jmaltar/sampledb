var passport = require('passport');
var mongoose = require('mongoose');
var gravatar = require('gravatar');
var Request = mongoose.model('Request');
var User = mongoose.model('User');

module.exports = function(io){
	var that = {};

	const _io = io;

	that.postRequest = function(req, res){
		
		var user_id = mongoose.Types.ObjectId(req.body.requestedBy);

		var scales = req.body.scales.split(',');

		var genres = req.body.genres.split(',');	

		var request = new Request({
			bpm: req.body.bpm,
			scales: scales,
			genres: genres,
			description: req.body.description,
			requestedBy: user_id
		});

		request.save(function(err){
			Request
				.findById(request._id)
				.populate('requestedBy', 'name gravatarUrl _id')
				.exec(function(err, newRequest){
					if(err){
						res.status(404).json("Not found");
					}
					_io.sockets.emit('request:new', newRequest);
					return res.status(200).send({
						message: "Success"
					});
				});				
		});
	}


	var parseProperties = function(currentPage, itemsPerPage, searchModel, reverse, genres, scales, bpm, callback){

		var searchPropertiesInfo = new Object({
			sortProperty: new Object(),
			currentPage: parseInt(currentPage),
			itemsPerPage: parseInt(itemsPerPage),
			scalesProperty: new Array(),
			genresProperty: new Array(),
			bpmProperty: undefined
		});

		searchPropertiesInfo.sortProperty[searchModel] = (reverse==="true") ? "desc" : "asc";

		if(scales.length === 1 && scales[0] === ""){
			scales = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B","Other"];
		}

		for(var s = 0; s < scales.length; s++){
			searchPropertiesInfo.scalesProperty.push(new Object({
				'scales': scales[s]
			}));
		} 

		if(genres.length === 1 && genres[0] === ""){
			genres = ["Blues", "Classical", "Country", "Electronic", "Folk", "Hip-hop", "Jazz", "Reggae", "Rock", "Traditional", "Pop", "Other"];
		}

		for(var g = 0; g < genres.length; g++){
			searchPropertiesInfo.genresProperty.push(new Object({
				'genres' : genres[g]
			}));
		}
		

		if(bpm){
			searchPropertiesInfo.bpmProperty = parseInt(bpm);
		} else {
			searchPropertiesInfo.bpmProperty = new Object({
				'$gte': 0
			});
		}	

		callback(searchPropertiesInfo);

	}

	that.getRequests = function(req, res){

		parseProperties(req.query.currentPage, req.query.itemsPerPage, req.query.searchModel, req.query.reverse, req.query.genres.split(','), req.query.scales.split(','), req.query.bpm, function(searchPropertiesInfo){

			Request
				.find({
					'$and': [
						{
							'$or': searchPropertiesInfo.scalesProperty
						},
						{
							'$or': searchPropertiesInfo.genresProperty
						}
					]
				})
				.where('bpm', searchPropertiesInfo.bpmProperty)
				.count(function(err, count){
					Request
						.find({
							'$and': [
								{
									'$or': searchPropertiesInfo.scalesProperty
								},
								{
									'$or': searchPropertiesInfo.genresProperty
								}
							]
						})
						.where('bpm', searchPropertiesInfo.bpmProperty)
						.sort(searchPropertiesInfo.sortProperty)
						.skip((searchPropertiesInfo.currentPage - 1)*searchPropertiesInfo.itemsPerPage)
						.limit(searchPropertiesInfo.itemsPerPage)
						.populate('requestedBy', 'name gravatarUrl _id')
						.exec(function(err, requests){
							if(err){
								res.status(404).json("Not found");
							}
							
							res.status(200).send({
								'requests': requests,
								'totalItems': count
							});
						});

				});

		});
		
	}


	that.totalItems = function(req, res){

		parseProperties(0, 0, "empty", "false", req.query.genres.split(','), req.query.scales.split(','), req.query.bpm, function(searchPropertiesInfo){

			Request
				.find({
					'$and': [
						{
							'$or': searchPropertiesInfo.scalesProperty
						},
						{
							'$or': searchPropertiesInfo.genresProperty
						}
					]
				})
				.where('bpm', searchPropertiesInfo.bpmProperty)
				.count()
				.exec(function(err, nr){
					res.status(200).send({
						totalItems: nr
					});
				});

		});
	}

	that.singleRequest = function(req, res){
		var requestId = mongoose.Types.ObjectId(req.params.request_id);
		Request
			.findOne({_id: requestId})
			.populate('requestedBy', 'name gravatarUrl _id')
			.exec(function(err, request){
				if(err){
					res.status(404).json({
						message: "Not found"
					})
				}
				res.status(200).send(request);
			});

	}

	that.getByUserId = function(req, res){

		var user_id = mongoose.Types.ObjectId(req.params.user_id);
		Request
			.find({requestedBy: user_id})
			.exec(function(err, requests){
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).send(requests);
			});	
	}

	that.deleteRequest = function(req, res){
		var r_id = mongoose.Types.ObjectId(req.params.request_id);
		if(req.payload._id){
			var user_id = req.payload._id;
			Request
				.findById(r_id)
				.exec(function(err, request){
					if(err){
						res.status(404).json("Not found");
					}
					if(request.requestedBy==user_id){
						Request
							.remove({_id: r_id}, function(err, removed){
								if(err){
									res.status(404).json("Not found");
								}
								_io.sockets.emit('request:remove', req.params.request_id);
								return res.status(200).send({
									message: "success"
								});
							});
					} else {
						res.status(401).json({
							"message": "Unauthorized access"
						});								
					}
				});			
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});											
		}
	}

	return that;
}
