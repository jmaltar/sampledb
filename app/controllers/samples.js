var mongoose = require('mongoose');
_ = require('lodash');

var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
var gfs = new Grid(mongoose.connection.db);
var User = mongoose.model('User');
var Message = mongoose.model('Message');
var Sample = mongoose.model('Sample');
var cookie = require('cookie');
var jsonwebtoken = require('jsonwebtoken');



module.exports = function(io, clients){
	var that = {};

	const _io = io;

	that.upload = function(req, res){


		var user_id = mongoose.Types.ObjectId(req.body.uploadedBy);

		var part = req.files.file;

		var scales = req.body.scales.split(',');
		
		var genres = req.body.genres.split(',');

		var writeStream = gfs.createWriteStream({
			filename: part.name,
			mode: 'w',
			content_type: part.mimetype,
			metadata: {
				definedname: req.body.definedname,
				bpm: req.body.bpm,
				scales: scales,
				genres: genres,
				description: req.body.description
			}
		});

		writeStream.on('close', function(file){
			var sample = new Sample({
				sampleInfo: file,	
				uploadedBy: user_id,
				sampleDownloadUrl: '/api/samples/download/'+file._id.toString(),
				sampleStreamUrl: '/api/samples/stream/'+file._id.toString()
			});
			sample.save(function(err){
				Sample
					.findById(sample._id)
					.populate('uploadedBy', 'name gravatarUrl _id')
					.exec(function(err, newSample){
						if(req.body.byRequestId){
							var message = new Message({
								from: user_id,
								to: mongoose.Types.ObjectId(req.body.byRequestUser),
								request: mongoose.Types.ObjectId(req.body.byRequestId),
								sample: sample._id,
								seen: false
							});
							message.save(function(err){
								Message
									.findById(message._id)
									.populate('from', 'name gravatarUrl _id')
									.populate('sample')
									.exec(function(err, newMessage){
										for(var x = 0; x< clients.length; x++){
											if(clients[x].customId == req.body.byRequestUser){
												clients[x].socket.emit('message', newMessage);
											}
										}
										_io.sockets.emit('sample:new', newSample);
										return res.status(200).send({
											message: "success"
										});								
									});
							});
						} else {
							_io.sockets.emit('sample:new', newSample);
							return res.status(200).send({
								message: "success"
							});
						}
					});
			});

		});

		writeStream.write(part.data);
		writeStream.end();

	}


	that.stream = function(req, res){

		if(req.headers.cookie){
			var cookies = cookie.parse(req.headers.cookie);
			if(cookies.jwt){
				jsonwebtoken.verify(cookies.jwt, 'MY_SECRET', function(err, decoded){
					if(err){
						res.status(401).json({
							"message": "Unauthorized access"
						});
					} else {
						var s_id = mongoose.Types.ObjectId(req.params.sample_id);
						gfs.files.findOne({_id: s_id}, function(err, file){
							if(err){
								res.status(404).json("Not found");
							}
							
							if(req.headers.range){

								var range = req.headers.range;
								var positions = range.replace(/bytes=/, "").split("-");
								var start = parseInt(positions[0], 10);
								var total = file.length;
								var end = positions[1]? parseInt(positions[1],10) : total - 1;
								var chunksize = (end-start)+1;

								res.writeHead(206, {
									"Content-Range": "bytes " + start + "-" + end + "/" + total,
									"Accept-Ranges": "bytes",
									"Content-Length": chunksize,
									"Content-Type": file.contentType
								});
								var readstream = gfs.createReadStream({
									_id: s_id,
									range: {
										startPos: start,
										endPos: end
									}			
								});

								readstream.pipe(res);

							} else {
								res.writeHead(200, {
									"Content-Type": "audio/mp3",
									"Content-Length": file.length
								});

								var readstream = gfs.createReadStream({
									_id: s_id
								});
								readstream.pipe(res);
							}
						});
					}
					
				});
			} else {
				res.status(401).json({
					"message": "Unauthorized access"
				});				
			}	
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});
		}
	}

	that.download = function(req, res){

		if(req.headers.cookie){
			var cookies = cookie.parse(req.headers.cookie);
			if(cookies.jwt){
				jsonwebtoken.verify(cookies.jwt, 'MY_SECRET', function(err, decoded){

					if(err){
						res.status(401).json({
							"message": "Unauthorized access"
						});				
					} else {
						var s_id = mongoose.Types.ObjectId(req.params.sample_id);
						gfs.files.findOne({_id: s_id}, function(err, file){
							if(err){
								res.status(404).json("Not found");
							}
							res.writeHead(200, {
								"Content-Type": "audio/mp3"
							});

							var readstream = gfs.createReadStream({
								_id: s_id
							});
							readstream.pipe(res);
						});
					}

				});				
			} else {
				res.status(401).json({
					"message": "Unauthorized access"
				});					
			}	
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});				
		}
	}


	var parseProperties = function(currentPage, itemsPerPage, searchModel, reverse, genres, scales, bpm, callback){

		var searchPropertiesInfo = new Object({
			sortProperty: new Object(),
			currentPage: parseInt(currentPage),
			itemsPerPage: parseInt(itemsPerPage),
			scalesProperty: new Array(),
			genresProperty: new Array(),
			bpmProperty: undefined
		});

		searchPropertiesInfo.sortProperty[searchModel] = (reverse==="true") ? "desc" : "asc";

		if(scales.length === 1 && scales[0] === ""){
			scales = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B","Other"];
		}

		for(var s = 0; s < scales.length; s++){
			searchPropertiesInfo.scalesProperty.push(new Object({
				'sampleInfo.metadata.scales': scales[s]
			}));
		} 

		if(genres.length === 1 && genres[0] === ""){
			genres = ["Blues", "Classical", "Country", "Electronic", "Folk", "Hip-hop", "Jazz", "Reggae", "Rock", "Traditional", "Pop", "Other"];
		}

		for(var g = 0; g < genres.length; g++){
			searchPropertiesInfo.genresProperty.push(new Object({
				'sampleInfo.metadata.genres' : genres[g]
			}));
		}
		

		if(bpm){
			searchPropertiesInfo.bpmProperty = bpm;
		} else {
			searchPropertiesInfo.bpmProperty = new Object({
				'$gte': "0"
			});
		}	

		callback(searchPropertiesInfo);
	
	}



	that.getSamples = function(req, res){

		parseProperties(req.query.currentPage, req.query.itemsPerPage, req.query.searchModel, req.query.reverse, req.query.genres.split(','), req.query.scales.split(','), req.query.bpm, function(searchPropertiesInfo){

			Sample
				.find({
					'$and': [
						{
							'$or': searchPropertiesInfo.scalesProperty
						},
						{
							'$or': searchPropertiesInfo.genresProperty
						}
					]
				})
				.where('sampleInfo.metadata.bpm', searchPropertiesInfo.bpmProperty)
				.count(function(err, count){
					Sample
						.find({
							'$and': [
								{
									'$or': searchPropertiesInfo.scalesProperty
								},
								{
									'$or': searchPropertiesInfo.genresProperty
								}
							]
						})
						.where('sampleInfo.metadata.bpm', searchPropertiesInfo.bpmProperty)
						.sort(searchPropertiesInfo.sortProperty)
						.skip((searchPropertiesInfo.currentPage - 1)*searchPropertiesInfo.itemsPerPage)
						.limit(searchPropertiesInfo.itemsPerPage)
						.populate('uploadedBy', 'name gravatarUrl _id')
						.exec(function(err, samples){
							if(err){
								res.status(404).json("Not found");
							}
							
							res.status(200).send({
								'samples': samples,
								'totalItems': count
							});
						});
				});

		});

	}

	that.totalItems = function(req, res){

		parseProperties(0, 0, "empty", "false", req.query.genres.split(','), req.query.scales.split(','), req.query.bpm, function(searchPropertiesInfo){
			Sample
				.find({
					'$and': [
						{
							'$or': searchPropertiesInfo.scalesProperty
						},
						{
							'$or': searchPropertiesInfo.genresProperty
						}
					]
				})	
				.where('sampleInfo.metadata.bpm', searchPropertiesInfo.bpmProperty)	
				.count()
				.exec(function(err, nr){
					res.status(200).send({
						totalItems: nr
					});
				});
		});
	}

	that.getByUserId = function(req, res){
		var user_id = mongoose.Types.ObjectId(req.params.user_id);
		Sample
			.find({uploadedBy: user_id})
			.exec(function(err, samples){
				if(err){
					res.status(404).json("Not found");
				}
				res.status(200).send(samples);
			});		
	}

	that.singleSample = function(req, res){
		var sampleId = mongoose.Types.ObjectId(req.params.sample_id);
		Sample
			.findOne({_id: sampleId})
			.populate('uploadedBy', 'name gravatarUrl _id')
			.exec(function(err, sample){
				if(err){
					res.status(404).json({
						message: "Not found"
					})
				}
				res.status(200).send(sample);
			});

	}

	that.remove = function(req, res){
		var sample_id = mongoose.Types.ObjectId(req.params.sample_id);	
		if(req.payload._id){
			var user_id = req.payload._id;
			Sample
				.findOne({'sampleInfo._id': sample_id})
				.exec(function(err, sample){
					if(err){
						res.status(404).json("Not found");
					}
					if(sample.uploadedBy==user_id){
						gfs.remove({_id: sample_id}, function(err){
							if(err){
								throw err;
							}

							Sample.remove({'sampleInfo._id': sample_id}, function(err, removed){
								if(err){
									res.status(404).json("Not found");
								}
								_io.sockets.emit('sample:remove', req.params.sample_id);
								return res.status(200).send({
									message: "success"
								});

							});

						});
					} else {
						res.status(401).json({
							"message": "Unauthorized access"
						});								
					}
				});
		} else {
			res.status(401).json({
				"message": "Unauthorized access"
			});					
		}
	}

	return that;
}


