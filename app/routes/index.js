var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});






module.exports = function(io, clients){
	
	var ctrlProfile = require('../controllers/profile')();
	var ctrlAuth = require('../controllers/authentication')(io);
	var ctrlRequest = require('../controllers/requests')(io);
	var ctrlSamples = require('../controllers/samples')(io, clients);
	var ctrlUsers = require('../controllers/users')(io);

	//profile
	router.get('/profile', auth, ctrlProfile.profileRead);
	router.delete('/profile/messages/:message_id', auth, ctrlProfile.deleteMessage);
	router.put('/profile/messages', auth, ctrlProfile.setSeenToMessage);	

	// authentication
	router.post('/register', ctrlAuth.register);
	router.post('/login', ctrlAuth.login);
	router.put('/changeemail/:user_id', auth, ctrlAuth.changeEmail);
	router.put('/changename/:user_id', auth, ctrlAuth.changeName);
	router.put('/changepassword/:user_id', auth, ctrlAuth.changePassword);
	router.get('/isuniqueemail/:email', ctrlAuth.isUniqueEmail);
	
	//requests
	router.post('/requests', auth, ctrlRequest.postRequest);
	router.get('/requests', ctrlRequest.getRequests);
	router.get('/requests/single/:request_id', auth, ctrlRequest.singleRequest);
	router.get('/requests/totalitems', ctrlRequest.totalItems);
	router.delete('/requests/:request_id', auth, ctrlRequest.deleteRequest);
	router.get('/requests/byuser/:user_id', auth, ctrlRequest.getByUserId);

	//samples
	router.post('/samples', auth, ctrlSamples.upload);
	router.get('/samples/single/:sample_id', auth, ctrlSamples.singleSample);
	router.get('/samples/stream/:sample_id', ctrlSamples.stream);
	router.get('/samples/download/:sample_id', ctrlSamples.download);
	router.delete('/samples/:sample_id', auth, ctrlSamples.remove);
	router.get('/samples', ctrlSamples.getSamples);
	router.get('/samples/totalitems', ctrlSamples.totalItems);
	router.get('/samples/byuser/:user_id', auth, ctrlSamples.getByUserId);

	//users
	router.get('/users', ctrlUsers.getUsers);
	router.get('/users/:user_id', auth, ctrlUsers.getUser);	

	return router;
}