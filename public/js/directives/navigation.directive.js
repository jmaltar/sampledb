(function () {

  angular
    .module('sampleDB')
    .directive('navigation', navigation);

  function navigation () {
    return {
      restrict: 'EA',
      templateUrl: '../views/navigation.template.html',
      controller: 'navigationCtrl'
    };
  }

})();