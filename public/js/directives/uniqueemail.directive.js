(function () {

  angular
    .module('sampleDB')
    .directive('uniqueEmail', uniqueEmail);

  uniqueEmail.$inject = ['authentication'];    
  function uniqueEmail (authentication) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel){
        element.bind('blur mouseleave', function(e){
          if(!ngModel||!element.val()){
            return;
          }
          var currentValue = element.val();
          authentication.isUniqueEmail(currentValue)
            .$promise
            .then(function(unique){
              if(currentValue==element.val()){
                ngModel.$setValidity('unique', unique.unique);
              }
            }, function(){
              ngModel.$setValidity('unique', true);
            });
        });
      }
    };
  }

})();