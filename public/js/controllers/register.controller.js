(function () {

  angular
    .module('sampleDB')
    .controller('registerCtrl', registerCtrl);

  registerCtrl.$inject = ['$scope', '$location', 'authentication', 'socket', '$uibModalInstance'];
  function registerCtrl($scope, $location, authentication, socket, $uibModalInstance) {

    $scope.credentials = {
      name : "",
      email : "",
      password : ""
    };

    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.onSubmit = function () {
      authentication.isUniqueEmail($scope.credentials.email)
        .$promise
        .then(function(unique){
          if(!unique.unique){
            toastr.error("Email is not unique"); 
            $scope.credentials.email = undefined; 
          } else {
            authentication
              .register($scope.credentials)
              .error(function(err){
                toastr.error("An error occurred");
              })
              .then(function(){
                $scope.cancel();
                socket.emit('storeClientInfo', {
                  customId: authentication.currentUser().id
                });
                toastr.success("Successfully registered");
                $location.path('profile');
              });
          }
        }, function(err){ 
          toastr.error("An error occurred");
        });        
    };

  }

})();