(function() {
  
  angular
    .module('sampleDB')
    .controller('homeCtrl', homeCtrl);
    homeCtrl.$inject = ['$scope', '$uibModal', 'authentication'];
    function homeCtrl ($scope, $uibModal, authentication) {

    	$scope.isLoggedIn = authentication.isLoggedIn();

	    $scope.openLoginModal = function(){
	      $uibModal.open({
	        templateUrl: '../views/modals/login.view.html',
	        animation: true,
	        backdrop: true,
	        windowClass: 'modal',
	        controller: 'loginCtrl'
	      });        
	    }      

	    $scope.openRegisterModal = function(){
	      $uibModal.open({
	        templateUrl: '../views/modals/register.view.html',
	        animation: true,
	        backdrop: true,
	        windowClass: 'modal',
	        controller: 'registerCtrl'
	      });        
	    }      
    }

})();