(function() {
  
  angular
    .module('sampleDB')
    .controller('uploadSampleCtrl', uploadSampleCtrl);

  uploadSampleCtrl.$inject = ['$scope', '$location','Upload', 'authentication', '$uibModalInstance', 'requestInfo', 'scalesFactory', 'genresFactory'];
  function uploadSampleCtrl($scope, $location, Upload, authentication, $uibModalInstance, requestInfo, scalesFactory, genresFactory) {

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;  
    }
    
    $scope.file = undefined;

    $scope.disableUploadButton = false;

    if(requestInfo){
      $scope.sampleInfo = new Object({
        definedname: undefined,
        bpm: angular.copy(requestInfo.bpm),
        scales: angular.copy(requestInfo.scales),
        genres: angular.copy(requestInfo.genres),
        description: undefined,
        uploadedBy: $scope.currentUserId,
        byRequestId: requestInfo._id,
        byRequestUser: requestInfo.requestedBy._id
      });

    } else {
      $scope.sampleInfo = new Object({
        definedname: undefined,
        bpm: undefined,
        scales: [],
        genres: [],
        description: undefined,
        uploadedBy: $scope.currentUserId
      });      
    }



    $scope.selectedScale = undefined;
    $scope.selectedGenre = undefined;
    
    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.submit = function(){
      $scope.disableUploadButton = true;
      $scope.upload($scope.file, $scope.sampleInfo);
    }

    $scope.upload = function (file, sampleInfo) { 

        var data = new Object({
            definedname: sampleInfo.definedname,
            bpm: sampleInfo.bpm===undefined ? 0 : sampleInfo.bpm,
            scales: sampleInfo.scales.length===0 ? "Other" : sampleInfo.scales.join(","),
            genres: sampleInfo.genres.length===0 ? "Other" : sampleInfo.genres.join(","),
            description: sampleInfo.description,
            uploadedBy: sampleInfo.uploadedBy   
        });

        if(sampleInfo.byRequestId && sampleInfo.byRequestUser){
          data.byRequestId = sampleInfo.byRequestId;
          data.byRequestUser = sampleInfo.byRequestUser;
        }

        Upload.upload({
            url: 'api/samples',
            data: data,
            headers: {
              Authorization: 'Bearer ' + authentication.getToken()
            },
            file: file
        }).then(function (resp) {
            $uibModalInstance.close();
            $scope.disableUploadButton = false;
            toastr.success("Sample successfully uploaded");
        }, function (resp) {
            console.log('Error status: ' + resp.status);
            toastr.error("An error occurred");
        }, function (evt) {
            $scope.progressPercentageString = parseInt(100.0 * evt.loaded / evt.total) + "%";
        });
    }; 

    $scope.scalesArray = scalesFactory;
    $scope.genresArray = genresFactory;

    $scope.appendScale = function(item){
      if($scope.sampleInfo.scales.indexOf(item)=== -1){
        $scope.sampleInfo.scales.push(item);
      }
      $scope.selectedScale = undefined;  
    }                          

    $scope.appendGenre = function(item){
      if($scope.sampleInfo.genres.indexOf(item)=== -1){
        $scope.sampleInfo.genres.push(item);
      }
      $scope.selectedGenre = undefined;
    }     

    $scope.removeScale = function(item){
      var index = $scope.sampleInfo.scales.indexOf(item);
      if(index >= 0){
        $scope.sampleInfo.scales.splice(index, 1);
      }
    }                     

    $scope.removeGenre = function(item){
      var index = $scope.sampleInfo.genres.indexOf(item);
      if(index >= 0){
        $scope.sampleInfo.genres.splice(index, 1);
      }
    }                         

  }

})();