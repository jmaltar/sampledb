(function() {
  
  angular
    .module('sampleDB')
    .controller('profileCtrl', profileCtrl);

  profileCtrl.$inject = ['$scope','$location', '$route', 'socket', 'profileData', '$uibModal', 'authentication'];
  function profileCtrl($scope, $location, $route, socket, profileData, $uibModal, authentication) {

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;      
    }    

    $scope.user = $route.current.locals.data.user;
    $scope.messages = $route.current.locals.data.messages;
    $scope.samplesByUser = $route.current.locals.samplesByUser;
    $scope.requestsByUser = $route.current.locals.requestsByUser;    

  	$scope.unreadMessagesFilter = function (item) { 
  	    return item.seen === false;
  	};

    socket.on('message', function(message){

    	$scope.messages.push(message);
    });	

    $scope.tabs = [
      {
        'heading': 'Inbox',
        'active': true,
        'template': '../views/tabs/inboxtab.view.html'
      },
      {
        'heading': 'Posts',
        'active': false,
        'template': '../views/tabs/poststab.view.html'
      }, 
      {
        'heading': 'Preferences',
        'active': false,
        'template': '../views/tabs/preferencestab.view.html'
      },            
      {
        'heading': 'Unread Messages',
        'active': true,
        'template': '../views/tabs/unreadmessagestab.view.html'
      },
      {
        'heading': 'All Messages',
        'active': false,
        'template': '../views/tabs/allmessagestab.view.html'
      },
      {
        'heading': 'Requests',
        'active': true,
        'template': '../views/tabs/userrequeststab.view.html'
      },
      {
        'heading': 'Samples',
        'active': false,
        'template': '../views/tabs/usersamplestab.view.html'
      }                
    ]; 
 
    $scope.tabsetClass = "col-md-8 col-md-offset-2";

    $scope.messagesReverse = false;

    $scope.setSeen = function(message){
    	profileData.setSeenToMessage(message.messageId, message)
    		.$promise
    		.then(function(data){
    			message.seen = true;
    		}, function(err){	
    			toastr.error("An error occurred");
    		});
    	
    }

    $scope.deleteMessage = function(messageId){
      profileData.deleteMessage(messageId)
        .$promise
        .then(function(data){
          $scope.messages = $.grep($scope.messages, function(message){
            return message._id != messageId;
          });           
          toastr.warning("Message deleted");         
        }, function(err){
          toastr.error("An error occurred");
        });
    }

    $scope.removeMessage = function(messageId){
      bootbox.confirm("Delete this message?", function(result){
        if(result){
          $scope.deleteMessage(messageId);
        }
      });     
    }

    $scope.messageClass = function(bool){
      return bool ? 'panel-info' : 'panel-primary';
    }

    $scope.unreadMessagesLength = function(){
      var count = $scope.messages.reduce(function(n, message){
        return n + (!message.seen)
      }, 0);

      return count;
    }

    $scope.messagesLength = function(){
      return $scope.messages.length;
    }

    socket.on('sample:new', function(newSample){
      if(newSample.uploadedBy._id===$scope.user._id){
        $scope.samplesByUser.push(newSample);
      }
    });

    socket.on('sample:remove', function(sampleId){
      $scope.samplesByUser = $.grep($scope.samplesByUser, function(sample){
        return sample.sampleInfo._id != sampleId;
      });
    });


    socket.on('request:new', function(newRequest){
      if(newRequest.requestedBy._id===$scope.user._id){
        $scope.requestsByUser.push(newRequest);  
      }
    });

    socket.on('request:remove', function(requestId){
      $scope.requestsByUser = $.grep($scope.requestsByUser, function(request){
        return request._id != requestId;
      });
    });


    $scope.removeSample = function(sampleId){
      bootbox.confirm("Delete this sample?", function(result){
        if(result){
          samplesData.deleteSample(sampleId)
            .$promise
            .then(function(data){
              toastr.warning("Sample deleted");
            }, function(err){
              toastr.error("An error occurred");
            });  
        }
      });     
    }

    $scope.removeRequest = function(requestId){
      bootbox.confirm("Delete this request?", function(result){
        if(result){
          requestData.deleteRequest(requestId)
            .$promise
            .then(function(data){
              toastr.warning("Request deleted");
            }, function(err){
              toastr.error("An error occurred");
            });
          
        }
      }); 
    }    

    $scope.openChangeNameModal = function(){
      $uibModal.open({
        templateUrl: '../views/modals/changenamemodal.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'preferencesCtrl'
      });
    }  

    $scope.openChangeEmailModal = function(){
      $uibModal.open({
        templateUrl: '../views/modals/changeemailmodal.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'preferencesCtrl'
      });
    }   

    $scope.openChangePasswordModal = function(){
      $uibModal.open({
        templateUrl: '../views/modals/changepasswordmodal.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'preferencesCtrl'
      });
    }          

    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.submit = function(){
      $scope.disableUploadButton = true;
      $scope.upload($scope.file, $scope.sampleInfo);
    }      

  } 

})();


