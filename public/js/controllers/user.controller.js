(function() {
  
  angular
    .module('sampleDB')
    .controller('userCtrl', userCtrl);

  userCtrl.$inject = ['userData', 'authentication', '$scope', '$route', 'socket', 'samplesData', 'requestData', '$uibModal'];
  function userCtrl(userData, authentication, $scope, $route, socket, samplesData, requestData, $uibModal) {

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;      
    }

    $scope.user = $route.current.locals.user;

    $scope.samplesByUser = $route.current.locals.samplesByUser;

    $scope.requestsByUser = $route.current.locals.requestsByUser;

    socket.on('sample:new', function(newSample){
      if(newSample.uploadedBy._id===$scope.user._id){
        $scope.samplesByUser.push(newSample);
      }
    });

    socket.on('sample:remove', function(sampleId){
      $scope.samplesByUser = $.grep($scope.samplesByUser, function(sample){
        return sample.sampleInfo._id != sampleId;
      });
    });


    socket.on('request:new', function(newRequest){
      if(newRequest.requestedBy._id===$scope.user._id){
        $scope.requestsByUser.push(newRequest);  
      }
    });

    socket.on('request:remove', function(requestId){
      $scope.requestsByUser = $.grep($scope.requestsByUser, function(request){
        return request._id != requestId;
      });
    });


    $scope.removeSample = function(sampleId){
      bootbox.confirm("Delete this sample?", function(result){
        if(result){
          samplesData.deleteSample(sampleId)
            .$promise
            .then(function(data){
              toastr.warning("Sample deleted");
            }, function(err){
              toastr.error("An error occurred");
            });  
        }
      });     
    }

    $scope.removeRequest = function(requestId){
      bootbox.confirm("Delete this request?", function(result){
        if(result){
          requestData.deleteRequest(requestId)
            .$promise
            .then(function(data){
              toastr.warning("Request deleted");
            }, function(err){
              toastr.error("An error occurred");
            });
          
        }
      }); 
    }    

    $scope.respondToRequest = function(requestInfo){
      $uibModal.open({
        templateUrl: '../views/modals/uploadsample.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'uploadSampleCtrl',
        resolve: {
          requestInfo: requestInfo
        }
      });
    }  

    $scope.tabs = [
      {
        'heading': 'Requests',
        'active': true,
        'template': '../views/tabs/userrequeststab.view.html'
      },
      {
        'heading': 'Samples',
        'active': false,
        'template': '../views/tabs/usersamplestab.view.html'
      }      
    ]; 
 
    $scope.tabsetClass = "col-md-8 col-md-offset-2";

  }

})();