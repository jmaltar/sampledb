(function() {
  
  angular
    .module('sampleDB')
    .controller('preferencesCtrl', preferencesCtrl);

  preferencesCtrl.$inject = ['$scope', '$location', '$window','$cookies','authentication', '$uibModalInstance', '$route'];
  function preferencesCtrl($scope, $location, $window,$cookies, authentication, $uibModalInstance, $route) {

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;  
    }
    
    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.changeUsername = function(){
      if($scope.newName){

        var info = new Object({
          newName: $scope.newName
        });
        authentication.changeName($scope.currentUserId, info)
          .$promise
          .then(function(data){
            $uibModalInstance.close();
            toastr.success("Username successfully updated");
            authentication.saveToken(data.token);
            $cookies.remove("jwt");
            authentication.setCookie(data.token);
            $route.reload();
          }, function(err){
            toastr.error("An error occurred");
          });
      }      
    }

    $scope.changePassword = function(){
      if($scope.newPassword){
        var info = new Object({
          newPassword: $scope.newPassword
        });
        authentication.changePassword($scope.currentUserId, info)
          .$promise
          .then(function(data){
            $uibModalInstance.close();
            toastr.success("Password successfully updated");
          }, function(err){
            toastr.error("An error occurred");
          });
      }      
    }    

    $scope.changeEmail = function(){
      if($scope.newEmail){
        authentication.isUniqueEmail($scope.newEmail)
          .$promise
          .then(function(unique){
            if(!unique.unique){
              toastr.error("Email is not unique"); 
              $scope.newEmail = undefined; 
            } else {
              var info = new Object({
                newEmail: $scope.newEmail
              });
              authentication.changeEmail($scope.currentUserId, info)
                .$promise
                .then(function(data){
                  $uibModalInstance.close();
                  toastr.success("Email successfully updated");
                  authentication.saveToken(data.token);
                  $cookies.remove("jwt");
                  authentication.setCookie(data.token);
                  $route.reload();
                }, function(err){
                  toastr.error("An error occurred");
                });

            }
          }, function(err){ 
            toastr.error("An error occurred");
          });
      }
    }            

  }

})();