(function() {
  
  angular
    .module('sampleDB')
    .controller('sampleCtrl', sampleCtrl);

  sampleCtrl.$inject = ['samplesData', 'authentication', '$location','$scope', '$route', 'socket'];
  function sampleCtrl(samplesData, authentication, $location, $scope, $route, socket) {
    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;  
    } else {
      toastr.warning("Please login to play and download this sample");
    }

    $scope.sample = $route.current.locals.sample;
    if(!$scope.sample._id){
      toastr.warning("Sample deleted. Going to all samples");
      $location.path('/samples');
    }    
    
    $scope.deleteSample = function(sampleId){
      samplesData.deleteSample(sampleId)
        .$promise
        .then(function(data){
          toastr.warning("Sample deleted");
        }, function(err){
          toastr.error("An error occurred");
        });        
    }
    
    $scope.removeSample = function(sampleId){
      bootbox.confirm("Delete this sample?", function(result){
        if(result){
          $scope.deleteSample(sampleId);
        }
      });     
    }


    socket.on('sample:remove', function(sampleId){
        if(sampleId===$scope.sample.sampleInfo._id){
          toastr.warning("Sample deleted. Going to all samples");
          $location.path('/samples');
        }
    });
  }


})();
