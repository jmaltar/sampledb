(function() {
  
  angular
    .module('sampleDB')
    .controller('requestCtrl', requestCtrl);

  requestCtrl.$inject = ['requestData', 'authentication', '$location','$scope', '$route', 'socket', '$uibModal'];
  function requestCtrl(requestData, authentication, $location, $scope, $route, socket, $uibModal) {
    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;  
    } else {
      toastr.warning("Please login to respond to this sample");
    }

    $scope.request = $route.current.locals.request;
    if(!$scope.request._id){
      toastr.warning("Request deleted. Going to all requests");
      $location.path('/requests');
    }
    
    $scope.deleteRequest = function(requestId){
      requestData.deleteRequest(requestId)
        .$promise
        .then(function(data){
          toastr.warning("Request deleted");
        }, function(err){
          toastr.error("An error occurred");
        });        
    }
    
    $scope.removeRequest = function(requestId){
      bootbox.confirm("Delete this request?", function(result){
        if(result){
          $scope.deleteRequest(requestId);
        }
      });     
    }


    socket.on('request:remove', function(requestId){
        if(requestId===$scope.request._id){
          toastr.warning("Request deleted. Going to all requests");
          $location.path('/requests');
        }
    });

    $scope.respondToRequest = function(requestInfo){
      $uibModal.open({
        templateUrl: '../views/modals/uploadsample.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'uploadSampleCtrl',
        resolve: {
          requestInfo: requestInfo
        }
      });
    }


  }


})();
