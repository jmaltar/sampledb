(function () {

  angular
  .module('sampleDB')
  .controller('loginCtrl', loginCtrl);

  loginCtrl.$inject = ['$scope', '$location', 'authentication', 'socket', '$uibModalInstance'];
  function loginCtrl($scope, $location, authentication, socket, $uibModalInstance) {

    $scope.credentials = {
      email : "",
      password : ""
    };

    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.onSubmit = function () {
      authentication
        .login($scope.credentials)
        .error(function(err){
          toastr.error("Wrong credentials");
        })
        .then(function(){ 
          $scope.cancel();
          socket.emit('storeClientInfo', {
            customId: authentication.currentUser().id
          });
          toastr.success("Successfully logged in");
          $location.path('profile');
        });
    };

  }

})();