(function() {
  
  angular
    .module('sampleDB')
    .controller('postRequestCtrl', postRequestCtrl);

  postRequestCtrl.$inject = ['$scope', 'authentication', '$uibModalInstance', 'requestData'];
  function postRequestCtrl($scope, authentication, $uibModalInstance, requestData) {

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;  
    }

    $scope.newRequest = new Object({
      bpm: undefined,
      scales: [],
      genres: [],
      description: undefined,
      requestedBy: $scope.currentUserId
    });

    $scope.selectedScale = undefined;
    $scope.selectedGenre = undefined;
    
    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.submit = function(){

      $scope.postRequest($scope.newRequest);
    }

    $scope.postRequest = function (newRequest) { 

        data = new Object({
          bpm: newRequest.bpm===undefined ? 0 : newRequest.bpm,
          scales: newRequest.scales.length===0 ? "Other" : newRequest.scales.join(","),
          genres: newRequest.genres.length===0 ? "Other" : newRequest.genres.join(","),
          description: newRequest.description,
          requestedBy: newRequest.requestedBy
        });
        requestData.postRequest(data)
          .$promise
          .then(function(data){
            $uibModalInstance.close();
            toastr.success("Request successfully posted");
          }, function(err){
            toastr.error("An error occurred");
          });
    }; 

    $scope.scalesArray = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B","Other"];
    $scope.genresArray = ["Blues", "Classical", "Country", "Electronic", "Folk", 
                          "Hip-hop", "Jazz", "Reggae", "Rock", "Traditional", "Pop", "Other"]

    $scope.appendScale = function(item){
      if($scope.newRequest.scales.indexOf(item)=== -1){
        $scope.newRequest.scales.push(item);
      }
      $scope.selectedScale = undefined;  
    }                          

    $scope.appendGenre = function(item){
      if($scope.newRequest.genres.indexOf(item)=== -1){
        $scope.newRequest.genres.push(item);
      }
      $scope.selectedGenre = undefined;
    }     

    $scope.removeScale = function(item){
      var index = $scope.newRequest.scales.indexOf(item);
      if(index >= 0){
        $scope.newRequest.scales.splice(index, 1);
      }
    }                     

    $scope.removeGenre = function(item){
      var index = $scope.newRequest.genres.indexOf(item);
      if(index >= 0){
        $scope.newRequest.genres.splice(index, 1);
      }
    }                         

  }

})();