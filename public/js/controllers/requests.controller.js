(function() {
  
  angular
    .module('sampleDB')
    .controller('requestsCtrl', requestsCtrl);

  requestsCtrl.$inject = ['$scope', 'requestData', 'authentication', '$route', 'socket', '$uibModal', '$anchorScroll', 'scalesFactory', 'genresFactory'];
  function requestsCtrl($scope, requestData, authentication, $route, socket, $uibModal, $anchorScroll, scalesFactory, genresFactory) {
    

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;      
    } else {
      $scope.currentUserId = undefined;
      toastr.warning("Please login to post and respond to requests");
    }
  
    $scope.requests = $route.current.locals.requests.requests;
    $scope.totalItems = $route.current.locals.requests.totalItems;

    $scope.sameArrays = function(array1, array2, callback){
      var is_same = (array1.length == array2.length) && array1.every(function(element, index) {
          return element._id === array2[index]._id; 
      });

      callback(is_same);      
    }

    $scope.queryRequests = function(currentPage, itemsPerPage, searchModel, reverse, filteredGenres, filteredScales, filteredBPM){
        requestData.getRequests(currentPage, itemsPerPage, searchModel, reverse, filteredGenres, filteredScales, filteredBPM)
          .$promise
          .then(function(data){
            $scope.sameArrays(data.requests, $scope.requests, function(is_same){
              if(!is_same){
                $scope.requests = data.requests;
              }
            });
            $scope.totalItems = data.totalItems;
          }, function(err){
            toastr.error("An error occurred");
          });         
    }

    $scope.deleteRequest = function(requestId){
      requestData.deleteRequest(requestId)
        .$promise
        .then(function(data){
          toastr.warning("Request deleted");
        }, function(err){
          toastr.error("An error occurred");
        });                   
    }

    socket.on('request:new', function(newRequest){
      $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);        
    });

    $scope.removeRequest = function(requestId){
      bootbox.confirm("Delete this request?", function(result){
        if(result){
          $scope.deleteRequest(requestId);          
        }
      });
    }

    socket.on('request:remove', function(requestId){
      $scope.requests = $.grep($scope.requests, function(request){
        return request._id != requestId;
      });

      requestData.totalItems($scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM)
        .$promise
        .then(function(totalItemsData){
          $scope.totalItems = totalItemsData.totalItems;
          if(($scope.currentPage * $scope.itemsPerPage) >= ($scope.totalItems + $scope.itemsPerPage)){
            $scope.currentPage--;
          } else {
            $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);
          }
        }, function(err){ 
          toastr.error("An error occurred");
        });

    });

    $scope.openPostRequestModal = function(){
      if(authentication.isLoggedIn()){
        $uibModal.open({
          templateUrl: '../views/modals/postrequest.view.html',
          animation: true,
          backdrop: true,
          windowClass: 'modal',
          controller: 'postRequestCtrl'
        });        
      } else {
        toastr.warning("Please login to post and respond to requests");
      }

    }

    $scope.respondToRequest = function(requestInfo){
      if(authentication.isLoggedIn()){
        $uibModal.open({
          templateUrl: '../views/modals/uploadsample.view.html',
          animation: true,
          backdrop: true,
          windowClass: 'modal',
          controller: 'uploadSampleCtrl',
          resolve: {
            requestInfo: requestInfo
          }
        });        
      } else {
        toastr.warning("Please login to post and respond to requests");        
      }

    }


    $scope.filteredScales = new Array();
    $scope.filteredGenres = new Array();
    $scope.filteredBPM = undefined; 

    $scope.scalesArray = scalesFactory;
    $scope.genresArray = genresFactory;

    $scope.bpmArray = new Array();
    for(var x = 0; x <= 200; x++){
      $scope.bpmArray[x] = x;
    }                          

    $scope.removeGenre = function(genre){
        var index = $scope.filteredGenres.indexOf(genre);
        if(index >= 0){
          $scope.filteredGenres.splice(index, 1);
        }            
    }

    $scope.removeScale = function(scale){
        var index = $scope.filteredScales.indexOf(scale);
        if(index >= 0){
          $scope.filteredScales.splice(index, 1);
        }        
    }

    $scope.removeBPM = function(){
      $scope.filteredBPM = undefined;
    }

    $scope.setFilteredBPM = function(bpm){
      $scope.filteredBPM = bpm;
    }

    $scope.setFilteredScale = function(scale){
      var newScalesArray = [];
      newScalesArray.push(scale);
      $scope.filteredScales = newScalesArray;
    }

    $scope.setFilteredGenre = function(genre){
      var newGenresArray = [];
      newGenresArray.push(genre);
      $scope.filteredGenres = newGenresArray;
    }

    $scope.$watch('selectedScale', function(){
      if($scope.selectedScale){
        if($scope.filteredScales.indexOf($scope.selectedScale)=== -1){
          $scope.filteredScales.push($scope.selectedScale);
        }         
        $scope.selectedScale = undefined;
      }
     
    });

    $scope.$watch('selectedGenre', function(){
      if($scope.selectedGenre){
        if($scope.filteredGenres.indexOf($scope.selectedGenre)=== -1){
          $scope.filteredGenres.push($scope.selectedGenre);
        }         
        $scope.selectedGenre = undefined;
      }
     
    });

    
    $scope.currentPage = 1;
    $scope.itemsPerPage = 6;
    $scope.maxSize = 5;

    $scope.dynamicPopover = {
      templateUrl: '../views/popover/searchtoolsrequests.view.html',
      title: 'Title'
    };


    $scope.searchModel = 'uploadDate';

    $scope.reverse = false; 

    $scope.initialReverseWatch = true;
    $scope.initialSearchModelWatch = true;
    $scope.initialCurrentPageWatch = true;
    $scope.initialItemsPerPageWatch = true;
    $scope.initialFilteredGenresWatch = true;
    $scope.initialFilteredScalesWatch = true;
    $scope.initialFilteredBPMWatch = true;

    $scope.$watch('reverse', function(){
      if($scope.initialReverseWatch){
        $scope.initialReverseWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);          
      }

    });

    $scope.$watch('searchModel', function(){
      if($scope.initialSearchModelWatch){
        $scope.initialSearchModelWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);                 
      }
    });

    $scope.$watch('currentPage', function(){
      if($scope.initialCurrentPageWatch){
        $scope.initialCurrentPageWatch = false;
      } else {
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);           
      }
    });

    $scope.$watch('itemsPerPage', function(){
      if($scope.initialItemsPerPageWatch){
        $scope.initialItemsPerPageWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);                     
      }
    });    


    $scope.$watch('filteredGenres', function(){
      if($scope.initialFilteredGenresWatch){
        $scope.initialFilteredGenresWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);                    
      }
    }, true);    

    $scope.$watch('filteredScales', function(){
      if($scope.initialFilteredScalesWatch){
        $scope.initialFilteredScalesWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);              
      }
    }, true);    

    $scope.$watch('filteredBPM', function(){
      if($scope.initialFilteredBPMWatch){
        $scope.initialFilteredBPMWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryRequests($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);                 
      }
    });

    $scope.gotoTop = function(){
      $anchorScroll();
    }

  }

})();