(function() {
  
  angular
    .module('sampleDB')
    .controller('aboutCtrl', aboutCtrl);

    aboutCtrl.$inject = ['$scope', '$uibModalInstance']

    function aboutCtrl ($scope, $uibModalInstance) {
	    $scope.cancel = function(){
	      $uibModalInstance.close();
	    }
    }
})();