(function() {
  
  angular
    .module('sampleDB')
    .controller('usersCtrl', usersCtrl);

  usersCtrl.$inject = ['userData', 'authentication', '$scope', '$route', 'socket', '$anchorScroll'];
  function usersCtrl(userData, authentication, $scope, $route, socket, $anchorScroll) {

    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;      
    } else {
      $scope.currentUserId = undefined;
      toastr.warning("Please login to view users profiles");
    }

    $scope.users = $route.current.locals.users.users;
    $scope.totalItems = $route.current.locals.users.totalItems;

    $scope.sameArrays = function(array1, array2, callback){
      var is_same = (array1.length == array2.length) && array1.every(function(element, index) {
          return element._id === array2[index]._id; 
      });

      callback(is_same);      
    }

    $scope.queryUsers = function(currentPage, itemsPerPage, searchModel, reverse){
      userData.getUsers(currentPage, itemsPerPage, searchModel, reverse)
        .$promise
        .then(function(data){
          $scope.sameArrays(data.users, $scope.users, function(is_same){
            if(!is_same){
              $scope.users = data.users;
            }
          });
          $scope.totalItems = data.totalItems;
        }, function(err){
          toastr.error("An error occurred");
        });        
    }

    socket.on('user:new', function(newUser){
        $scope.queryUsers($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse);
    }); 

    $scope.currentPage = 1;
    $scope.itemsPerPage = 6;
    $scope.maxSize = 5;

    $scope.dynamicPopover = {
      templateUrl: '../views/popover/searchtoolsusers.view.html',
      title: 'Title'
    };


    $scope.searchModel = 'name';

    $scope.reverse = false; 

    $scope.initialReverseWatch = true;
    $scope.initialSearchModelWatch = true;
    $scope.initialCurrentPageWatch = true;
    $scope.initialItemsPerPageWatch = true;

    $scope.$watch('reverse', function(){
      if($scope.initialReverseWatch){
        $scope.initialReverseWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryUsers($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse);
      }

    });

    $scope.$watch('searchModel', function(){
      if($scope.initialSearchModelWatch){
        $scope.initialSearchModelWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryUsers($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse);
      }
    });

    $scope.$watch('currentPage', function(){
      if($scope.initialCurrentPageWatch){
        $scope.initialCurrentPageWatch = false;
      } else {
        $scope.queryUsers($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse);
      }
    });

    $scope.$watch('itemsPerPage', function(){
      if($scope.initialFilteredBPMWatch){
        $scope.initialFilteredBPMWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.queryUsers($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse);
      }
    });    



    $scope.gotoTop = function(){
      $anchorScroll();
    }           

  }

})();