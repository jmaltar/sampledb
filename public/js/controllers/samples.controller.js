(function() {
  
  angular
    .module('sampleDB')
    .controller('samplesCtrl', samplesCtrl);

  samplesCtrl.$inject = ['samplesData', 'authentication', '$scope', '$route', 'socket', '$uibModal', '$anchorScroll', 'scalesFactory', 'genresFactory'];
  function samplesCtrl(samplesData, authentication, $scope, $route, socket, $uibModal, $anchorScroll, scalesFactory, genresFactory) {
    if(authentication.isLoggedIn()){
      $scope.currentUserId = authentication.currentUser().id;  
    } else {
      $scope.currentUserId = undefined;
      toastr.warning("Please login to play, download and upload samples");
    }

    $scope.samples = $route.current.locals.samples.samples;
    $scope.totalItems = $route.current.locals.samples.totalItems

    $scope.sameArrays = function(array1, array2, callback){
      var is_same = (array1.length == array2.length) && array1.every(function(element, index) {
          return element._id === array2[index]._id; 
      });

      callback(is_same);      
    }

    $scope.querySamples = function(currentPage, itemsPerPage, searchModel, reverse, filteredGenres, filteredScales, filteredBPM){
      samplesData.getSamples(currentPage, itemsPerPage, searchModel, reverse, filteredGenres, filteredScales, filteredBPM)
        .$promise
        .then(function(data){
          $scope.sameArrays(data.samples, $scope.samples, function(is_same){
            if(!is_same){
              $scope.samples = data.samples;             
            }
            $scope.totalItems = data.totalItems; 
          });
        }, function(err){
          toastr.error("An error occurred");
        });        
    }

    $scope.deleteSample = function(sampleId){
      samplesData.deleteSample(sampleId)
        .$promise
        .then(function(data){
          toastr.warning("Sample deleted");
        }, function(err){
          toastr.error("An error occurred");
        });        
    }

    socket.on('sample:new', function(newSample){
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);
    });
    
    $scope.removeSample = function(sampleId){
      bootbox.confirm("Delete this sample?", function(result){
        if(result){
          $scope.deleteSample(sampleId);
        }
      });     
    }

    $scope.showWarning = function(){
      toastr.warning("Please login to play, download and upload samples");
    }

    socket.on('sample:remove', function(sampleId){
      $scope.samples = $.grep($scope.samples, function(sample){
        return sample.sampleInfo._id != sampleId;
      });

      samplesData.totalItems($scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM)
        .$promise
        .then(function(totalItemsData){
          $scope.totalItems = totalItemsData.totalItems;
          if(($scope.currentPage * $scope.itemsPerPage) >= ($scope.totalItems + $scope.itemsPerPage)){
            $scope.currentPage--;
          } else {
            $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);
          }
        }, function(err){ 
          toastr.error("An error occurred");
        });
    });

    $scope.openUploadSampleModal = function(){
      if(authentication.isLoggedIn()){
        $uibModal.open({
          templateUrl: '../views/modals/uploadsample.view.html',
          animation: true,
          backdrop: true,
          windowClass: 'modal',
          controller: 'uploadSampleCtrl',
          resolve: {
            requestInfo: undefined
          }
        });        
      } else {
        toastr.warning("Please login to play, download and upload samples");
      }

    }

        
    $scope.filteredScales = new Array();
    $scope.filteredGenres = new Array();
    $scope.filteredBPM = undefined; 

    $scope.scalesArray = scalesFactory;
    $scope.genresArray = genresFactory;

    $scope.bpmArray = new Array();
    for(var x = 0; x <= 200; x++){
      $scope.bpmArray[x] = x;
    }                          

    $scope.removeGenre = function(genre){
        var index = $scope.filteredGenres.indexOf(genre);
        if(index >= 0){
          $scope.filteredGenres.splice(index, 1);
        }            
    }

    $scope.removeScale = function(scale){
        var index = $scope.filteredScales.indexOf(scale);
        if(index >= 0){
          $scope.filteredScales.splice(index, 1);
        }        
    }

    $scope.removeBPM = function(){
      $scope.filteredBPM = undefined;
    }

    $scope.setFilteredBPM = function(bpm){
      $scope.filteredBPM = bpm;
    }

    $scope.setFilteredScale = function(scale){
      var newScalesArray = new Array();
      newScalesArray.push(scale);
      $scope.filteredScales = newScalesArray;
    }

    $scope.setFilteredGenre = function(genre){
      var newGenresArray = new Array();
      newGenresArray.push(genre);
      $scope.filteredGenres = newGenresArray;
    }

    $scope.$watch('selectedScale', function(){
      if($scope.selectedScale){
        if($scope.filteredScales.indexOf($scope.selectedScale)=== -1){
          $scope.filteredScales.push($scope.selectedScale);
        }         
        $scope.selectedScale = undefined;
      }
     
    });

    $scope.$watch('selectedGenre', function(){
      if($scope.selectedGenre){
        if($scope.filteredGenres.indexOf($scope.selectedGenre)=== -1){
          $scope.filteredGenres.push($scope.selectedGenre);
        }         
        $scope.selectedGenre = undefined;
      }
     
    });

    
    $scope.currentPage = 1;
    $scope.itemsPerPage = 6;
    $scope.maxSize = 5;

    $scope.dynamicPopover = {
      templateUrl: '../views/popover/searchtoolssamples.view.html',
      title: 'Title'
    };


    $scope.searchModel = 'sampleInfo.uploadDate';

    $scope.reverse = false; 

    $scope.initialReverseWatch = true;
    $scope.initialSearchModelWatch = true;
    $scope.initialCurrentPageWatch = true;
    $scope.initialItemsPerPageWatch = true;
    $scope.initialFilteredGenresWatch = true;
    $scope.initialFilteredScalesWatch = true;
    $scope.initialFilteredBPMWatch = true;

    $scope.$watch('reverse', function(){
      if($scope.initialReverseWatch){
        $scope.initialReverseWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);          
      }

    });

    $scope.$watch('searchModel', function(){
      if($scope.initialSearchModelWatch){
        $scope.initialSearchModelWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);            
      }
    });

    $scope.$watch('currentPage', function(){
      if($scope.initialCurrentPageWatch){
        $scope.initialCurrentPageWatch = false;
      } else {
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);
      }
    });

    $scope.$watch('itemsPerPage', function(){
      if($scope.initialItemsPerPageWatch){
        $scope.initialItemsPerPageWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);           
      }
    });    


    $scope.$watch('filteredGenres', function(){
      if($scope.initialFilteredGenresWatch){
        $scope.initialFilteredGenresWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);
      }
    }, true);    

    $scope.$watch('filteredScales', function(){
      if($scope.initialFilteredScalesWatch){
        $scope.initialFilteredScalesWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM); 
      }
    }, true);    

    $scope.$watch('filteredBPM', function(){
      if($scope.initialFilteredBPMWatch){
        $scope.initialFilteredBPMWatch = false;
      } else {
        $scope.currentPage = 1;
        $scope.querySamples($scope.currentPage, $scope.itemsPerPage, $scope.searchModel, $scope.reverse, $scope.filteredGenres.join(','), $scope.filteredScales.join(','), $scope.filteredBPM);       
      }
    });


    $scope.gotoTop = function(){
      $anchorScroll();
    }
  }


})();

