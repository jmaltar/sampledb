(function () {

  angular
    .module('sampleDB')
    .controller('navigationCtrl', navigationCtrl);

  navigationCtrl.$inject = ['$scope', '$location','authentication', '$route', 'socket', '$uibModal'];
  function navigationCtrl($scope, $location, authentication, $route, socket, $uibModal) {
    
    $scope.isLoggedIn = authentication.isLoggedIn();

    $scope.currentUser = authentication.currentUser();

    $scope.logout = function(){
      socket.emit('removeClientInfo', {
        customId: authentication.currentUser().id
      });
    	authentication.logout();
      toastr.info("Goodbye");
      if($route.current.$$route.originalPath == "/"){
        $route.reload();
      } else {
        $location.path('/');
      }
    }

    $scope.openAboutModal = function(){
      $uibModal.open({
        templateUrl: '../views/modals/about.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'aboutCtrl'
      });        
    }    

    $scope.openLoginModal = function(){
      $uibModal.open({
        templateUrl: '../views/modals/login.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'loginCtrl'
      });        
    }      

    $scope.openRegisterModal = function(){
      $uibModal.open({
        templateUrl: '../views/modals/register.view.html',
        animation: true,
        backdrop: true,
        windowClass: 'modal',
        controller: 'registerCtrl'
      });        
    }     

  }

})();