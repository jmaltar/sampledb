(function () {

  angular
    .module('sampleDB')
    .service('socket', socket);

  socket.$inject = ['$rootScope', 'authentication'];
  function socket ($rootScope, authentication) {

    var socket = io.connect();

    socket.on('connect', function(){
      if(authentication.isLoggedIn()){
        socket.emit('storeClientInfo', {
          customId: authentication.currentUser().id
        });
      }
    });

    socket.on('message', function(message){
      var string = "User <strong>" + message.from.name + "</strong> responded to your request "+
                    "with sample <strong>" + message.sample.sampleInfo.metadata.definedname + "</strong>";
      toastr.info(string);
    });

    socket.on('sample:new', function(newSample){
      if(newSample.uploadedBy._id !== authentication.currentUser().id){
        var string = "User <strong>" + newSample.uploadedBy.name + "</strong> "+
                      "uploaded new sample <strong>" + newSample.sampleInfo.metadata.definedname + "</strong>"                    
        toastr.info(string);        
      }
    });    

    socket.on('request:new', function(newRequest){
      if(newRequest.requestedBy._id !== authentication.currentUser().id){
        var string = "User <strong>" + newRequest.requestedBy.name + "</strong> "+
                    "post new request"
        toastr.info(string);
      }
    });

    var on = function(eventName, callback){
      socket.on(eventName, function(){
        var args = arguments;
        $rootScope.$apply(function(){
          callback.apply(socket, args)
        });
      });
    }

    var emit = function(eventName, data, callback){
      socket.emit(eventName, data, function(){
        var args = arguments;
        $rootScope.$apply(function(){
          if(callback){
            callback.apply(socket, args);
          }
        });
      });
    }
    

    return {
      on : on,
      emit: emit
    };
  }


})();