(function() {

  angular
    .module('sampleDB')
    .service('requestData', requestData);

  requestData.$inject = ['$resource', 'authentication'];
  function requestData ($resource, authentication) {

    var postRequest = function (newRequest) {
      var resource = $resource('/api/requests', {}, {
        post: {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        }
      });
      return resource.post(newRequest);
    };


    var getRequests = function(currentPage, itemsPerPage, searchModel, reverse, genres, scales, bpm){
      var resource = $resource('/api/requests', {
        currentPage: '@currentPage',
        itemsPerPage: '@itemsPerPage',
        searchModel: '@searchModel',
        reverse: '@reverse',
        genres: '@genres',
        scales: '@scales',
        bpm: '@bpm'
      }, {
        get: {
          method: 'GET'
        }
      })
      return resource.get({
        currentPage: currentPage, 
        itemsPerPage: itemsPerPage,
        searchModel: searchModel,
        reverse: reverse,
        genres: genres,
        scales: scales,
        bpm: bpm
      });
    }    

    var totalItems = function(genres, scales, bpm){
      var resource = $resource('/api/requests/totalitems', {
        genres: '@genres',
        scales: '@scales',
        bpm: '@bpm'
      }, {
        get: {
          method: 'GET'
        }
      })
      return resource.get({
        genres: genres,
        scales: scales,
        bpm: bpm
      });
    }        

    var getRequest = function(request_id){
      var resource = $resource('/api/requests/single/:request_id', {
        request_id: '@request_id'
      }, {
        get: {
          method: 'GET',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        }
      });
      return resource.get({request_id: request_id});
    }

    var getRequestsByUserId = function(user_id){
      var resource = $resource('/api/requests/byuser/:user_id', {
        user_id: '@user_id'
      }, {
        query: {
          method: 'GET',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          },
          isArray: true
        }
      });
      return resource.query({user_id: user_id})
    }

    var deleteRequest = function(request_id){
      var resource = $resource('/api/requests/:request_id', {
        request_id: '@request_id'
        
      }, {
        delete: {
          method: 'DELETE',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        }
      });

      return resource.delete({request_id: request_id});
    }


    return {
      postRequest: postRequest,
      getRequests: getRequests,
      getRequest: getRequest,
      totalItems: totalItems,
      getRequestsByUserId: getRequestsByUserId,
      deleteRequest: deleteRequest
    };
  }

})();