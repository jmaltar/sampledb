(function () {

  angular
    .module('sampleDB')
    .service('authentication', authentication);

  authentication.$inject = ['$http', '$resource', '$window', '$cookies'];
  function authentication ($http, $resource, $window, $cookies) {

    var saveToken = function (token) {
      $window.localStorage['mean-token'] = token;
    };

    var getToken = function () {
      return $window.localStorage['mean-token'];
    };

    var isLoggedIn = function() {
      var token = getToken();
      var payload;

      if(token){
        payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    };

    var currentUser = function() {
      if(isLoggedIn()){
        var token = getToken();
        var payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);
        return {
          id: payload._id,
          email : payload.email,
          name : payload.name
        };
      }
    };

    var setCookie = function(token){
      var expiry = new Date();
      expiry.setDate(expiry.getDate()+7);
      $cookies.put("jwt", token, {
        expires: expiry
      });
    }

    var register = function(user) {
      return $http.post('/api/register', user).success(function(data){
        saveToken(data.token);
        setCookie(data.token);
      });
    };

    var login = function(user) {
      return $http.post('/api/login', user).success(function(data) {
        saveToken(data.token);        
        setCookie(data.token);
      });
    };


    var changeEmail = function(user_id, info){
      var resource = $resource('/api/changeemail/:user_id', null, {
        update: {
          method: 'PUT',
          headers: {
            Authorization: 'Bearer ' + getToken()
          }                   
        }
      });

      return resource.update({user_id: user_id}, info);
    }

    var changeName = function(user_id, info){
      var resource = $resource('/api/changename/:user_id', null, {
        update: {
          method: 'PUT',
          headers: {
            Authorization: 'Bearer ' + getToken()
          }                   
        }
      });

      return resource.update({user_id: user_id}, info);
    }

    var changePassword = function(user_id, info){
      var resource = $resource('/api/changepassword/:user_id', null, {
        update: {
          method: 'PUT',
          headers: {
            Authorization: 'Bearer ' + getToken()
          }                   
        }
      });

      return resource.update({user_id: user_id}, info);
    }

    logout = function() {
      $window.localStorage.removeItem('mean-token');
      $cookies.remove("jwt");
    };

    isUniqueEmail = function(email){
      var resource = $resource('/api/isuniqueemail/:email', {
        email: '@email'
      });

      return resource.get({email: email});
    };  

    return {
      currentUser : currentUser,
      saveToken : saveToken,
      getToken : getToken,
      isLoggedIn : isLoggedIn,
      register : register,
      setCookie: setCookie,
      login : login,
      logout : logout,
      isUniqueEmail: isUniqueEmail,
      changeEmail: changeEmail,
      changeName: changeName,
      changePassword: changePassword
    };
  }


})();