(function() {

  angular
    .module('sampleDB')
    .service('samplesData', samplesData);

  samplesData.$inject = ['$http', '$resource', 'authentication'];
  function samplesData ($http, $resource, authentication) {

    
    var getSamples = function(currentPage, itemsPerPage, searchModel, reverse, genres, scales, bpm){
      var resource = $resource('/api/samples', {
        currentPage: '@currentPage',
        itemsPerPage: '@itemsPerPage',
        searchModel: '@searchModel',
        reverse: '@reverse',
        genres: '@genres',
        scales: '@scales',
        bpm: '@bpm'
      }, {
        get: {
          method: 'GET'
        }
      })
      return resource.get({
        currentPage: currentPage, 
        itemsPerPage: itemsPerPage,
        searchModel: searchModel,
        reverse: reverse,
        genres: genres,
        scales: scales,
        bpm: bpm
      });
    }    

    var totalItems = function(genres, scales, bpm){
      var resource = $resource('/api/samples/totalitems', {
        genres: '@genres',
        scales: '@scales',
        bpm: '@bpm'
      }, {
        get: {
          method: 'GET'
        }
      })
      return resource.get({
        genres: genres,
        scales: scales,
        bpm: bpm
      });
    }        

    var getSample = function(sample_id){
      var resource = $resource('/api/samples/single/:sample_id', {
        sample_id: '@sample_id'
      }, {
        get: {
          method: 'GET',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        }
      });
      return resource.get({sample_id: sample_id});
    }

    var getSamplesByUserId = function(user_id){
      var resource = $resource('/api/samples/byuser/:user_id', {
        user_id: '@user_id'
      }, {
        query: {
          method: 'GET',
          headers: {
            Authorization: 'Bearer '+authentication.getToken()
          },
          isArray: true
        }
      });

      return resource.query({user_id: user_id})
    }

    var deleteSample = function(sample_id){
      var resource = $resource('/api/samples/:sample_id', {
        sample_id: '@sample_id'
      }, {
        delete: {
          method: 'DELETE',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        }
      });

      return resource.delete({sample_id: sample_id});
    }

    return {
      getSamples: getSamples,
      totalItems: totalItems,
      getSample: getSample,
      getSamplesByUserId: getSamplesByUserId,
      deleteSample: deleteSample      
    };
  }

})();