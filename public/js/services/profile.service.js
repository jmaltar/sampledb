(function() {

  angular
    .module('sampleDB')
    .service('profileData', profileData);

  profileData.$inject = ['$http', '$resource', 'authentication'];
  function profileData ($http, $resource, authentication) {

    var getProfile = function(){
      var resource = $resource('/api/profile', {}, {
        get: {
          method: 'GET',
          headers: {
            Authorization: 'Bearer '+ authentication.getToken()
          }
        }
      });

      return resource.get();
    }

    var setSeenToMessage = function(message_id, message){
      var resource = $resource('/api/profile/messages/:message_id', null, {
        update: {
          method: 'PUT',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }                   
        }
      });

      return resource.update({message_id: message_id}, message);
    }

    var deleteMessage = function(message_id){
      var resource = $resource('/api/profile/messages/:message_id', {
        message_id: '@message_id'
      }, {
        delete: {
          method: 'DELETE',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken(),
            userId: authentication.currentUser().id
          }
        }
      });

      return resource.delete({message_id: message_id});
    }

    return {
      getProfile : getProfile,
      setSeenToMessage: setSeenToMessage,
      deleteMessage: deleteMessage
    };
  }

})();