(function() {

  angular
    .module('sampleDB')
    .service('userData', userData);

  userData.$inject = ['$resource', 'authentication'];
  function userData ($resource, authentication) {



    var getUsers = function(currentPage, itemsPerPage, searchModel, reverse){
      var resource = $resource('/api/users', {
        currentPage: '@currentPage',
        itemsPerPage: '@itemsPerPage',
        searchModel: '@searchModel',
        reverse: '@reverse'
      }, {
        get: {
          method: 'GET'
        }
      });

      return resource.get({
        currentPage: currentPage, 
        itemsPerPage: itemsPerPage,
        searchModel: searchModel,
        reverse: reverse
      });
    }    


    var getUser = function(user_id){
      var resource = $resource('/api/users/:user_id', {
        user_id: '@user_id'
      }, {
        get: {
          method: 'GET',
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        }
      });
      return resource.get({user_id: user_id});
    }

    return {
      getUsers: getUsers,
      getUser: getUser
    };
  }

})();