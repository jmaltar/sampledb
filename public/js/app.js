(function(){
	angular.module('sampleDB', ['ngRoute', 'ngResource', 'ngCookies', 'ngAnimate', 'ngMaterial', 'ngAria','ui.bootstrap', 'ngFileUpload', 'wu.masonry']);

	function config($routeProvider, $locationProvider){
		$routeProvider
			.when('/', {
				templateUrl: '../views/home.view.html',
				controller: 'homeCtrl'
			})
			.when('/profile', {
				templateUrl: '../views/profile.view.html',
				controller: 'profileCtrl',
				resolve: {
					data: function($route, profileData){
						return profileData.getProfile().$promise;
					},
					samplesByUser: function($route, samplesData, authentication){
						return samplesData.getSamplesByUserId(authentication.currentUser().id).$promise;	
					},
					requestsByUser: function($route, requestData, authentication){
						return requestData.getRequestsByUserId(authentication.currentUser().id).$promise;		
					}					
				}								
			})
			.when('/requests', {
				templateUrl: '../views/requests.view.html',
				controller: 'requestsCtrl',
				resolve: {
					requests: function($route, requestData){
						return requestData.getRequests(1, 6, 'uploadDate', false, "", "", undefined).$promise;
					}
				}
			})
			.when('/requests/:request_id', {
				templateUrl: '../views/request.view.html',
				controller: 'requestCtrl',
				resolve: {
					request: function($route, requestData){
						return requestData.getRequest($route.current.pathParams.request_id).$promise;
					}
				}
			})				
			.when('/samples', {
				templateUrl: '../views/samples.view.html',
				controller: 'samplesCtrl',
				resolve: {
					samples: function($route, samplesData){
						return samplesData.getSamples(1, 6, 'sampleInfo.uploadDate', false, "", "", undefined).$promise;
					}
				}
			})
			.when('/samples/:sample_id', {
				templateUrl: '../views/sample.view.html',
				controller: 'sampleCtrl',
				resolve: {
					sample: function($route, samplesData){
						return samplesData.getSample($route.current.pathParams.sample_id).$promise;
					}
				}
			})			
			.when('/users', {
				templateUrl: '../views/users.view.html',
				controller: 'usersCtrl',
				resolve: {
					users: function($route, userData){
						return userData.getUsers(1, 6, 'name', false).$promise;
					}
				}
			})
			.when('/users/:user_id', {
				templateUrl: '../views/user.view.html',
				controller: 'userCtrl',
				resolve: {
					user: function($route, userData){
						return userData.getUser($route.current.pathParams.user_id).$promise;
					},
					samplesByUser: function($route, samplesData){
						return samplesData.getSamplesByUserId($route.current.pathParams.user_id).$promise;	
					},
					requestsByUser: function($route, requestData){
						return requestData.getRequestsByUserId($route.current.pathParams.user_id).$promise;		
					}
				}
			})
			.otherwise({redirectTo: '/'});
		$locationProvider.html5Mode(true);
	}

	function run($rootScope, $location, authentication){
		$rootScope.$on('$routeChangeStart', function(event, nextRoute, currentRoute){
			var path = nextRoute.$$route.originalPath;

			if(path === '/profile' && !authentication.isLoggedIn()) {
				$location.path('/');
			}

			if(path === '/users/:user_id'){
				if(authentication.isLoggedIn()){
					if(nextRoute.params.user_id == authentication.currentUser().id){
						$location.path('/profile');
					}
				} else {
					toastr.warning("Please login to view users profiles");
					$location.path('/users');
				}
			}

			if(path === '/samples/:sample_id'){
				if(!authentication.isLoggedIn()){
					toastr.warning("Please login to play, download and upload samples");
					$location.path('/samples');
				}
			}			

			if(path === '/requests/:request_id'){
				if(!authentication.isLoggedIn()){
					toastr.warning("Please login to post and respond to requests");
					$location.path('/requests');
				}
			}				

		});
	}

	angular
		.module('sampleDB')
		.config(['$routeProvider', '$locationProvider', config])
		.run(['$rootScope', '$location', 'authentication', run]);
})();