(function() {

  angular
    .module('sampleDB')
    .factory('genresFactory', genresFactory);

  
  function genresFactory () {
    var genres = ["Blues", "Classical", "Country", "Electronic", "Folk", "Hip-hop", "Jazz", "Reggae", "Rock", "Traditional", "Pop", "Other"];

    return genres;
  }

})();