(function() {

  angular
    .module('sampleDB')
    .factory('scalesFactory', scalesFactory);

  
  function scalesFactory () {
    var scales = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B","Other"];

    return scales;
  }

})();